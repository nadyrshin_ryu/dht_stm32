//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stdlib.h>
#include <string.h>
#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_gpio.h"
#include "main.h"
#include "delay\delay.h"
#include "dht\dht.h"
#include "hd44780\hd44780.h"


float temp, rh;

void main()
{
  SystemInit();

  hd44780_init();
  
  while (1)
  {
    if (dht_read_data() == DHT_ERR_Ok)
    {
      temp = dht_get_temperature();
      rh = dht_get_humidity();
    }
    
    hd44780_goto_xy(0, 0);
    hd44780_printf("TEMP = %d * \r\nRH   = %d %% ", (char)temp, (char) rh);

    delay_ms(1000);
  }
}
