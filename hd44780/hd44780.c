//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stm32f10x_gpio.h>
#include <stm32f10x_rcc.h>
#include "..\delay\delay.h"
#include "hd44780.h"
#include "cp1251toepson.h"
#include "cp866toepson.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>


// ������� ��� ���������� ��������
#define HD44780_RS_HIGH()       GPIO_SetBits(HD44780_RS_Port, HD44780_RS_Pin)
#define HD44780_RS_LOW()        GPIO_ResetBits(HD44780_RS_Port, HD44780_RS_Pin)
#define HD44780_RW_HIGH()       GPIO_SetBits(HD44780_RW_Port, HD44780_RW_Pin)
#define HD44780_RW_LOW()        GPIO_ResetBits(HD44780_RW_Port, HD44780_RW_Pin)
#define HD44780_E_HIGH()        GPIO_SetBits(HD44780_E_Port, HD44780_E_Pin)
#define HD44780_E_LOW()         GPIO_ResetBits(HD44780_E_Port, HD44780_E_Pin)

char LastRow = 0;                                               // ��������� ������������� ������
char hd44780_StrBuff[HD44780_COLS * HD44780_ROWS + 8];          // ����� ������ ��� ������


//==============================================================================
// ������� ������ ����� �� hd44780
// - IsCmd - ������ ���������� ����� (����� ����� ������)
//==============================================================================
unsigned char hd44780_read(char IsCmd)
{
  unsigned char Data = 0;
  unsigned short PortData;
  if (IsCmd)
    HD44780_RS_LOW();
  else
    HD44780_RS_HIGH();
  
  HD44780_RW_HIGH();
  
#if HD44780_4bitMode
  // ��������� ������� �������
  HD44780_E_HIGH();
  delay_us(HD44780_ShortDelayUs);
  PortData = GPIO_ReadInputData(HD44780_Data_Port);
  Data = ((PortData >> HD44780_Data_Shift) & 0xF) << 4;
  HD44780_E_LOW();

  delay_us(HD44780_ShortDelayUs);

  // ��������� ������� �������
  HD44780_E_HIGH();
  delay_us(HD44780_ShortDelayUs);
  PortData = GPIO_ReadInputData(HD44780_Data_Port);
  Data |= (PortData >> HD44780_Data_Shift) & 0xF;
  HD44780_E_LOW();
#else
  HD44780_E_HIGH();
  delay_us(HD44780_ShortDelayMks);
  // ��������� ���� � ���� ������
  Data = HD44780_Data_Pin;
  HD44780_E_LOW();
#endif
  
  HD44780_RW_LOW();

  return Data;
}
//==============================================================================


//==============================================================================
// ��������� ������ ����� �� hd44780
// - IsCmd - ������ ���������� ����� (����� ����� ������)
//==============================================================================
void hd44780_write(unsigned char Data, char IsCmd)
{
  GPIO_InitTypeDef InitStruct;
  InitStruct.GPIO_Speed = GPIO_Speed_50MHz;

  if (IsCmd)
    HD44780_RS_LOW();
  else
    HD44780_RS_HIGH();
  
#if HD44780_4bitMode
  // ����������� ���� ������ ��� ������
  InitStruct.GPIO_Pin = 0xF << HD44780_Data_Shift;
  InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init(HD44780_Data_Port, &InitStruct);
  
  // ����� ������� �������
  GPIO_ResetBits(HD44780_Data_Port, 0xF << HD44780_Data_Shift);
  GPIO_SetBits(HD44780_Data_Port, (Data >> 4) << HD44780_Data_Shift);
  HD44780_E_HIGH();
  delay_us(HD44780_ShortDelayUs);
  HD44780_E_LOW();

  delay_us(HD44780_ShortDelayUs);

  // ����� ������� �������
  GPIO_ResetBits(HD44780_Data_Port, 0xF << HD44780_Data_Shift);
  GPIO_SetBits(HD44780_Data_Port, (Data & 0xF) << HD44780_Data_Shift);
  HD44780_E_HIGH();
  delay_us(HD44780_ShortDelayUs);
  HD44780_E_LOW();
  
  // ����������� ���� ������ ��� �����
  InitStruct.GPIO_Mode = GPIO_Mode_IPD;
  GPIO_Init(HD44780_Data_Port, &InitStruct);

#else
  // ����������� ���� ������ ��� ������
  InitStruct.GPIO_Pin = 0xFF;
  InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_Init(HD44780_Data_Port, &InitStruct);
  
  // ����� ����
  GPIO_ResetBits(HD44780_Data_Port, 0xFF);
  GPIO_SetBits(HD44780_Data_Port, Data);
  HD44780_E_HIGH();
  delay_us(HD44780_ShortDelayUs);
  HD44780_E_LOW();
  
  // ����������� ���� ������ ��� �����
  InitStruct.GPIO_Mode = GPIO_Mode_IPD;
  GPIO_Init(HD44780_Data_Port, &InitStruct);
#endif
}
//==============================================================================


//==============================================================================
// ������� �������� ������������ ����������� ������� (���� HD44780_WaitBisyFlag=1)
//==============================================================================
#if (HD44780_WaitBisyFlag)
unsigned char hd44780_waitbisy(unsigned char tick)
{
  while ((hd44780_read(1) & 0x80) && (tick)) 
  {
    tick--;
  }
  
  return (tick) ? 0 : 1;
}
#endif
//==============================================================================


//==============================================================================
// ��������� ������ ������� � ��������� ������������ ����������� �������
//==============================================================================
void hd44780_write_cmd(unsigned char Data)
{
#if (HD44780_WaitBisyFlag)
  hd44780_waitbisy(100);
#endif
  
  hd44780_write(Data, 1);

#if (!HD44780_WaitBisyFlag)
  delay_us(HD44780_BisyDelayUs);
#endif
}
//==============================================================================


//==============================================================================
// ��������� ������ ����� ������ � ��������� ������������ ����������� �������
//==============================================================================
void hd44780_write_data(unsigned char Data)
{
#if (HD44780_WaitBisyFlag)
  hd44780_waitbisy(100);
#endif

  hd44780_write(Data, 0);

#if (!HD44780_WaitBisyFlag)
  delay_us(HD44780_BisyDelayUs);
#endif
}
//==============================================================================


//==============================================================================
// ��������� ������ ������������ GPIO
//==============================================================================
void PortClockStart(GPIO_TypeDef *GPIOx)
{
  if (GPIOx == GPIOA)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
  else if (GPIOx == GPIOB)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
  else if (GPIOx == GPIOC)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
  else if (GPIOx == GPIOD)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD, ENABLE);
  else if (GPIOx == GPIOE)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);
  else if (GPIOx == GPIOF)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOF, ENABLE);
  else if (GPIOx == GPIOG)
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOG, ENABLE);
}
//==============================================================================


//==============================================================================
// ��������� ������������� ��� �� ��� ������ � hd44780
//==============================================================================
void hd44780_bus_init(void)
{
  // �������� ������������ ��������� ������
  PortClockStart(HD44780_RS_Port);
  PortClockStart(HD44780_RW_Port);
  PortClockStart(HD44780_E_Port);
  PortClockStart(HD44780_Data_Port);
  
  // ��� ����, ����������� ����� ����������� ��� ������
  GPIO_InitTypeDef InitStruct;
  InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
  InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  // ������ E
  InitStruct.GPIO_Pin = HD44780_E_Pin;
  GPIO_Init(HD44780_E_Port, &InitStruct);
  GPIO_ResetBits(HD44780_E_Port, HD44780_E_Pin);
  // ������ RS
  InitStruct.GPIO_Pin = HD44780_RS_Pin;
  GPIO_Init(HD44780_RS_Port, &InitStruct);
  GPIO_ResetBits(HD44780_RS_Port, HD44780_RS_Pin);

#if (HD44780_WaitBisyFlag)
  // ������ RW
  InitStruct.GPIO_Pin = HD44780_RW_Pin;
  GPIO_Init(HD44780_RW_Port, &InitStruct);
#else
  InitStruct.GPIO_Pin = HD44780_RW_Pin;
  GPIO_Init(HD44780_RW_Port, &InitStruct);
  GPIO_ResetBits(HD44780_RW_Port, HD44780_RW_Pin);
#endif
  
#if HD44780_4bitMode
  InitStruct.GPIO_Mode = GPIO_Mode_IPD;
  InitStruct.GPIO_Pin = 0xF << HD44780_Data_Shift;
  GPIO_Init(HD44780_Data_Port, &InitStruct);
#else
  InitStruct.GPIO_Mode = GPIO_Mode_IPD;
  InitStruct.GPIO_Pin = 0xFF;
  GPIO_Init(HD44780_Data_Port, &InitStruct);
#endif
}
//==============================================================================


//==============================================================================
// ��������� ������������� ������� (�������� ������������������ ������ � �������)
//==============================================================================
void hd44780_start(void)
{
#if HD44780_4bitMode
  unsigned char Reg = 0x20;
#else
  unsigned char Reg = 0x30;
#endif

#if (HD44780_ROWS > 1)
  Reg |= 0x08;
#endif
  
  hd44780_write_cmd(Reg);       // ������ �������, 8��� ���� ������
  hd44780_write_cmd(0x0C);      // �������� �������
  hd44780_write_cmd(0x06);      // ������������� ������
  hd44780_clear();              // ������� ������
}
//==============================================================================


//==============================================================================
// ��������� ������������� �������
//==============================================================================
void hd44780_init(void)
{
  hd44780_bus_init();
  delay_ms(100);
  hd44780_start();
}
//==============================================================================


//==============================================================================
// ��������� ��������� ������� �������
//==============================================================================
void hd44780_goto_xy(char Row, char Col)
{
  // ��������� ��������� ����� ������ ������
  unsigned char Adr = 0;            
  if (Row & 1)                  // �������� ������ � �.�. hd44780 - ������ ������
    Adr = 0x40;                 // ������ �������� �� ������ ������ hd44780
  if (Row > 1)                  // ��� 4-��������� ������� ��� ������ ������ > 2
    Adr += HD44780_COLS;        // ������ ����� ������ �� ����� ������

  // ��������� ����� � ������
  Adr += Col;
  
  // ����� ���������� ����� DRAM � hd44780
  hd44780_write_cmd(Adr | 0x80);        // ������� ��������� ������ � DRAM
  
  LastRow = Row;
}
//==============================================================================


//==============================================================================
// ��������� ������� �������
//==============================================================================
void hd44780_clear(void)
{
  hd44780_write_cmd(0x01);      // ������� ������

#if (!HD44780_WaitBisyFlag)
  delay_ms(2);
#endif
}
//==============================================================================


//==============================================================================
// ��������� ������ � ������� ������� ����
//==============================================================================
void hd44780_write_buff(char *pBuff, char Len)
{
  while (Len--)
  {
    hd44780_write_data(*(pBuff++));
  }
}
//==============================================================================


//==============================================================================
// ��������� �������� ANSI-������ � ������� ������� �������
//==============================================================================
void hd44780_puts(char *str)
{
  char i;
  
  while (*str != '\0')
  {
    switch (*str)
    {
    case '\n':  // ������� �� ����� ������
      LastRow++;
      hd44780_goto_xy(LastRow, 0);
      break;
    case '\r':
      hd44780_goto_xy(LastRow, 0);
      break;
    case '\t':
      for (i = 0; i < 4; i++)
        hd44780_write_data(0x20);
      break;
    default:
        hd44780_write_data(*str);
      break;
    }
    str++;
  }
}
//==============================================================================


//==============================================================================
// ��������� ���������������� ������ ������� � ������� ������� �������
//==============================================================================
void hd44780_printf(const char *args, ...)
{
  va_list ap;
  va_start(ap, args);
  char len = vsnprintf(hd44780_StrBuff, sizeof(hd44780_StrBuff), args, ap);
  va_end(ap);

  // ��������������� ������� �������� � ��������� EPSON ��� �������� hd44780
#if (SOURCE_CODEPAGE == CP1251)
  cp1251_to_epson_convert(hd44780_StrBuff);
#else
  cp866_to_epson_convert(hd44780_StrBuff);
#endif
  
  hd44780_puts(hd44780_StrBuff);
}
//==============================================================================
